#include "queenwidget.h"

/*
 * This is my first QT Game which is created
 * by Viktor Bagány.
*/

QueenWidget::QueenWidget(QWidget *parent) : QWidget(parent)
{
    setMinimumSize(600, 600);
    setBaseSize(600,600);
    setWindowTitle(trUtf8("8 királynő"));

    label = new QLabel(trUtf8("Válassz táblaméretet:"));
    label->setAlignment(Qt::AlignHCenter);

    newGameButton = new QPushButton();
    newGameButton->setText(trUtf8("Játék indítása"));
    connect(newGameButton, SIGNAL(clicked()), this, SLOT(NewGameButtonClicked()));

    numberOfBoxes = new QSpinBox();
    SetupGame();

    buttonContainer = new QGridLayout();
    GenerateGameTable();

    mainView = new QVBoxLayout();
    mainView->addWidget(label);
    mainView->addWidget(numberOfBoxes);
    mainView->addWidget(newGameButton);

    ingame = false;
    setLayout(mainView);
}

void QueenWidget::StartGame()
{
    queens = 0;
    ingame = true;
    label->hide();
    numberOfBoxes->hide();
    stepCount = 0;

    newGameButton->setText(trUtf8("Játék vége"));
    if(!addedButtonLayout)
    {
        mainView->addLayout(buttonContainer);
        addedButtonLayout = true;
    }
    for(int i=0; i<numberOfBoxes->value(); i++)
    {
        for(int j=0; j<numberOfBoxes->value(); j++)
        {
            buttonData[i][j]->show();
            buttonData[i][j]->setEnabled(true);
        }
    }
    ResetView();
}

void QueenWidget::StopGame()
{
    ingame = false;
    msgBox.setText(buttonText);
    msgBox.exec();
    BlockAndHideAllButtons();
    numberOfBoxes->show();
    label->show();
    newGameButton->setText(trUtf8("Játék indítása"));
}

void QueenWidget::StepGame(int &x, int &y)
{
    if(dataTable[x][y] != 0)
    {
    /*
         * Levesszük a királynőt a tábláról
    */
        for(int i=0; i<8; i++)
        {
            dataTable[x][i]-=2;
            dataTable[i][y]-=2;

            if(x+i < 8 && y+i < 8)
                dataTable[x+i][y+i] -=2;
            if(x-i >= 0 && y-i >= 0)
                dataTable[x-i][y-i] -=2;
            if(x-i >= 0 && y+i < 8)
                dataTable[x-i][y+i] -=2;
            if(x+i < 8 && y-i >= 0)
                dataTable[x+i][y-i] -=2;
        }
        dataTable[x][y] = 0;
        queens--;
    }
    else
    {
    /*
         * Feltesszük a királynőt a táblára
    */
        for(int i=0; i<8; i++)
        {
            dataTable[x][i]+=2;
            dataTable[i][y]+=2;

            if(x+i < 8 && y+i < 8)
                dataTable[x+i][y+i] +=2;
            if(x-i >= 0 && y-i >= 0)
                dataTable[x-i][y-i] +=2;
            if(x-i >= 0 && y+i < 8)
                dataTable[x-i][y+i] +=2;
            if(x+i < 8 && y-i >= 0)
                dataTable[x+i][y-i] +=2;
        }
        dataTable[x][y] = 1;
        queens++;
    }
    stepCount++;
}

void QueenWidget::SetupGame()
{
    numberOfBoxes->setMinimum(4);
    numberOfBoxes->setMaximum(8);
    numberOfBoxes->setSingleStep(2);
}

void QueenWidget::NewGameButtonClicked()
{
    if(ingame)
    {
        buttonText = QString(trUtf8("Megszakítottad a játékot. Lépések száma: %1 ")).arg(stepCount);
        StopGame();
    } else {
        StartGame();
    }
}
void QueenWidget::GenerateGameTable()
{
    dataTable = new int*[8];
    buttonData.resize(8);
    for(int i=0; i<8;i++)
    {
        dataTable[i] = new int[8];
        buttonData[i].resize(8);
        for(int j=0; j<8;j++)
        {
            dataTable[i][j] = 0;
            buttonData[i][j] = new QPushButton();
            buttonData[i][j]->setFont(QFont("Times New Roman", 60, QFont::Bold));
            buttonData[i][j]->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
            buttonData[i][j]->hide();
            buttonContainer->addWidget(buttonData[i][j],i,j);
            connect(buttonData[i][j],SIGNAL(clicked(bool)),this,SLOT(ButtonClicked()));
        }
    }
}

void QueenWidget::ButtonClicked()
{
    QPushButton* senderButton = dynamic_cast <QPushButton*> (QObject::sender());
    int location = buttonContainer->indexOf(senderButton);
    int x = location / 8;
    int y = location % 8;

    StepGame(x,y);
    UpdateGameLayout();

    if(queens == numberOfBoxes->value())
    {
        buttonText = QString(trUtf8("Megnyerted a játékot! Lépések száma: %1 ")).arg(stepCount);
        StopGame();
        StartGame();
    }
}
void QueenWidget::UpdateGameLayout()
{
    for(int i=0; i<8; ++i)
    {
        for(int j=0; j<8; j++)
        {
            if(dataTable[i][j] < 0)
                dataTable[i][j] = 0;

            if(dataTable[i][j] >= 2)
            {
                buttonData[i][j]->setText(trUtf8("X"));
                //buttonData[i][j]->setStyleSheet("background-color: red");
                buttonData[i][j]->setEnabled(false);
            }
            if(dataTable[i][j] == 1)
            {
                buttonData[i][j]->setText(trUtf8("♕"));
            }
            if(dataTable[i][j] == 0)
            {
                buttonData[i][j]->setText(trUtf8(""));
                buttonData[i][j]->setEnabled(true);
                buttonData[i][j]->setStyleSheet("");
            }
        }
    }
}
void QueenWidget::ResetView()
{
    for(int i=0; i<8; i++)
    {
        for(int j=0; j<8; j++)
        {
            dataTable[i][j] = 0;
        }
    }
    UpdateGameLayout();
}
void QueenWidget::BlockAndHideAllButtons()
{
    for(int i=0; i<8; i++)
    {
        for(int j=0; j<8; j++)
        {
            buttonData[i][j]->setEnabled(false);
            buttonData[i][j]->hide();
        }
    }
}

