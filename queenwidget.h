#ifndef QUEENWIDGET_H
#define QUEENWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QSpinBox>
#include <QLabel>
#include <QPixmap>
#include <QIcon>
#include <QMessageBox>
#include <QString>

class QueenWidget : public QWidget
{
    Q_OBJECT

    public:
        QueenWidget(QWidget* parent = 0);

    private slots:
        void ButtonClicked();
        void NewGameButtonClicked();

    private:
        void NewGame();
        void GenerateGameTable();
        void UpdateGameLayout();
        void ResetView();
        void BlockAndHideAllButtons();
        void StopGame();
        void StartGame();
        void SetupGame();
        void StepGame(int& x, int& y);

        int stepCount;
        int queens;
        int** dataTable;
        bool addedButtonLayout;
        bool ingame;
        QLabel* label;
        QString buttonText;
        QGridLayout* buttonContainer;
        QVBoxLayout* mainView;
        QSpinBox* numberOfBoxes;
        QPushButton* newGameButton; // új játék gombja
        QVector<QVector<QPushButton*>> buttonData; // gombtábla
        QMessageBox msgBox;
};

#endif // QUEEN_H
