#-------------------------------------------------
#
# Project created by QtCreator 2018-09-22T13:33:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 8kiralyno
TEMPLATE = app

SOURCES += main.cpp \
    queenwidget.cpp

HEADERS  += \
    queenwidget.h

FORMS    +=
